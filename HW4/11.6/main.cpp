#include <bits/stdc++.h>
using namespace std;
class soccer
{
   public:
   string name; //player's name
   int num; //player's number
   int points; //player's points  
};
main()
{
   soccer s[12];
   for(int i=0;i<12;i++)
   {
       cout<<"Enter the name of player "<<i+1<<": ";
       cin>>s[i].name;
       cout<<"Enter the number of player "<<i+1<<": ";
       cin>>s[i].num;
       if(s[i].num<0)
       {
           cout<<"\nEnter posititve values for number of player: ";
           cin>>s[i].num;
       }
       cout<<"Enter the points scored by player "<<i+1<<": ";
       cin>>s[i].points;
       if(s[i].points<0)
       {
           cout<<"\nEnter posititve values for points scored by player: ";
           cin>>s[i].points;
       }
   }
   //display the information of each player
   cout<<"------------------------------------------\n";
   cout<<"Player name\tPlayer number\tPlayer points\n";
   for(int i=0;i<12;i++)
   {
       cout<<s[i].name<<"\t\t"<<s[i].num<<"\t\t"<<s[i].points<<"\n";
   }
   //calculate total points
   int total_points=0;
   for(int i=0;i<12;i++)
   {
       total_points+=s[i].points;
   }
   cout<<"\nThe total points earned by the team is: "<<total_points;
   int max=-1; //max variable to store the maximum point
   int index=-1; //index variable to store the index of player who scored maximum points
   for(int i=0;i<12;i++)
   {
       if(s[i].points>max)
       {
           max=s[i].points;
           index=i;
       }
   }
   cout<<"\nThe name and number of the player who scored maximum points are respectively "<<s[index].name<<" and "<<s[index].num;
}