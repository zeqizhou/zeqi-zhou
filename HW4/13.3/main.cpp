#include<iostream>
#include<string>

using namespace std;

class Car
{
private:
int year;
string make;
int speed = 0;

public:
Car( int year, const std::string &make );
int getYear();
string getMake();
int getSpeed();
void accelerate();
void brake();


};

Car::Car( int y, const std::string &m ) {
year = y;
make = m;
speed = 0;
}

int Car::getYear(){
return year;}

string Car::getMake(){
return make;}

int Car::getSpeed(){
return speed;}

void Car::accelerate()
{speed +=5;}

void Car::brake()
{
if( speed > 5 )
speed -=5;
else speed = 0 ;
}


int main()
{
Car myCar(2015,"ASDF");
int i = 0;
for (; i<5; ++i)
{
myCar.accelerate();
cout << "Accelerating.\n" << "The current speed of the car is: " << myCar.getSpeed() << endl;


}
{
int j = 0;
for (; j<5; ++j)
{
myCar.brake();
cout << "Decelerating.\n" << "The current speed of the car is: " << myCar.getSpeed()<<endl;

}

}
}