#include <iostream>

using namespace std;

class Employee {

private:

string name;

int idNumber;

string department;

string position;

public:

Employee(string n, int i, string d, string p) {

name = n;

idNumber = i;

department = d;

position = p;

}

Employee(string n, int i) {

name = n;

idNumber = i;

department = "";

position = "";

}

Employee() {

name = "";

idNumber = 0;

department = "";

position = "";

}

void setName(string n) {

name = n;

}

void setIdNumber(int i) {

idNumber = i;

}

void setDepartment(string d) {

department = d;

}

void setPosition(string p) {

position = p;

}

string getName() {

return name;

}

int getIdNumber() {

return idNumber;

}

string getDepartment() {

return department;

}

string getPosition() {

return position;

}

};

int main() {

// creating objects

Employee e1("Susan Meyers", 47899, "Accounting", "Vice President");

Employee e2("Mark Jones", 39119);

e2.setDepartment("IT");

e2.setPosition("Programmer");

Employee e3;

e3.setName("Joy Rogers");

e3.setIdNumber(81774);

e3.setDepartment("Manufacturing");

e3.setPosition("Engineer");

cout<<e1.getName()<<", "<<e1.getIdNumber()<<", "<<e1.getDepartment()<<", "<<e1.getPosition()<<endl;

cout<<e2.getName()<<", "<<e2.getIdNumber()<<", "<<e2.getDepartment()<<", "<<e2.getPosition()<<endl;

cout<<e3.getName()<<", "<<e3.getIdNumber()<<", "<<e3.getDepartment()<<", "<<e3.getPosition()<<endl;

return 0;

}