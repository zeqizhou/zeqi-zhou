#include <iostream>
using namespace std;

char monthWeatherArray[12][10] = { "January", "February", "March", "April", "May","June", "July", "August", "September", "October","November", "December" };

struct Weather
{
   double totalRainfall;
   double highTemperature;
   double lowTemperature;  
   double averageTemperature;  
};

double weatherArrayAverage(Weather weatherArray [], char c[])
{
   double average = 0;  


   if(c == "temp")
   {
  
       for(int i = 0; i < 12; i++)
       {
           average = average + weatherArray[i].averageTemperature;
       }
  
       return (average / 12);
   }


   if(c == "rain")
   {
  
       for(int i = 0; i < 12; i++)
       {
           average = average + weatherArray[i].totalRainfall;
       }
  
       return (average / 12);
   }
}

void getUserData(Weather weatherArray[])
{
   for(int i = 0; i < 12; i++)
   {
  
       cout << "From " << monthWeatherArray[i] << ": \n\n";
       cout << "Total rainfall: ";
       cin >> weatherArray[i].totalRainfall;

  
       while(weatherArray[i].totalRainfall < 0)
       {
           cout << "\nInvaid Input\n\n";
           cout << "Total rainfall: ";
           cin >> weatherArray[i].totalRainfall;
       }

       cout << "Highest temperature: ";
       cin >> weatherArray[i].highTemperature;
  
       while((weatherArray[i].highTemperature < -100) or (weatherArray[i].highTemperature > 140))
       {
           cout << "\nInvalid Input\n";
           cout << "Highest temperature: ";
           cin >> weatherArray[i].highTemperature;
       }

       cout << "Lowest temperature: ";
       cin >> weatherArray[i].lowTemperature;
  
       while((weatherArray[i].lowTemperature < -100) or (weatherArray[i].lowTemperature > 140) or (weatherArray[i].lowTemperature > weatherArray[i].highTemperature))
       {
           cout << "\nInvaid Input\n\n";
           cout << "Lowest temperature: ";
           cin >> weatherArray[i].lowTemperature;
       }

       weatherArray[i].averageTemperature = (weatherArray[i].highTemperature + weatherArray[i].lowTemperature)/2;

       cout << endl;
   }
}

int main()
{

   Weather weatherArray[12];

   getUserData(weatherArray);  

   int maximumposition = 0;
   int minimumposition = 0;  

   double minimum = weatherArray[0].lowTemperature;
   double maximum = weatherArray[0].highTemperature;  

   for(int i = 1; i < 12; i++)
   {
  
       if(minimum > weatherArray[i].lowTemperature)
       {
           minimum = weatherArray[i].lowTemperature;
           minimumposition = i;
       }
      
  
       if(maximum < weatherArray[i].highTemperature)
       {
           maximum = weatherArray[i].highTemperature;
           maximumposition = i;
       }
   }


   cout << "Average rainfall: " << weatherArrayAverage(weatherArray, "rain") << endl;
   cout << "Average temperature: " << weatherArrayAverage(weatherArray, "temp") << endl;
   cout << "Highest temperature: " << weatherArray[maximumposition].highTemperature << " (" << monthWeatherArray[maximumposition] << ")" << endl;
   cout << "Lowest temperature: " << weatherArray[minimumposition].lowTemperature << " (" << monthWeatherArray[minimumposition] << ")" << endl;


   return 0;
}