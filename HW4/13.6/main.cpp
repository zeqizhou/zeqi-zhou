#include <iostream>
 #include <cstdlib>
using namespace std;
#include <string>  
// InventoryItem class declaration.
class InventoryItem
{
private:
string desc; // The item desc
int iitemno;// item number
double cost; // The item cost
double tcost;//total cost
int quantity; // Number of units on hand
public:
InventoryItem()
{
desc="";
iitemno=0;
cost=0.0;
tcost=0.0;
quantity=0;
}
// Constructor
InventoryItem(string desc,int num, double c, int u)
{

// Copy the desc to the allocated memory.
desc= desc;
iitemno=num;
// Assign values to cost and units.
cost = c;
quantity = u;}
  
void setIitemno(int num)
{
iitemno=num;
}
int getIitemno()
{
return iitemno;
}
// Mutator functions
void setdesc(string des )
{ desc= des; }
string getdesc()
{ return desc; }
void setCost(double co)
{
cost=co;
settcost();
}
double getCost() const
{ return cost;
}
void settcost()
{
tcost=cost*quantity;
}
double gettcost()
{
return tcost;
}
void setQuantity(int qu)

{
quantity=qu;
}
int getquantity() const
{ return quantity; }
};
int main()
{
// Create an InventoryItem object and call
// the default constructor.
InventoryItem item1;
item1.setdesc("Hammer"); // Set the desc
item1.setIitemno(2);
item1.setCost(6.95); // Set the cost
item1.setQuantity(12); // Set the units
cout<<"Item 1:"<<endl;
cout<<"desc:"<<item1.getdesc()<<endl;
cout<<"Item Number:"<<item1.getIitemno()<<endl;
cout<<"Quantity:"<<item1.getCost()<<endl;
cout<<"Cost:"<<item1.getCost()<<endl;
cout<<"Total Cost:"<<item1.gettcost()<<endl;
//pause system for a while
system("pause");
}//end main