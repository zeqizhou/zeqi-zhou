#include<iostream>

#include<iomanip>

#include<cstring>

#include<string>


using namespace std;


// Static member variable type string to translate from integer to month-day format


// Class declaration

class DayOfYear

{

public:

int day;

string Month;


// Constructor function

DayOfYear(int dayEntered)

{

day = dayEntered;



} // end constructor function


void print()

{

if(day >= 1 && day <= 31)

{

cout <<" Day "<<day<<" would be " <<"January " << day << endl;

}

if(day >= 32 && day <= 59)

{
cout <<" Day "<<day<<" would be " <<"February "<< day-31 << endl;


}


if(day >= 60 && day <= 90)

{
cout <<" Day "<<day<<" would be " <<"March"<< day-59 << endl;


}


if(day >= 91 && day <= 120)

{
cout <<" Day "<<day<<" would be " <<"April"<< day-90 << endl;


}


if(day >= 121 && day <= 151)

{
cout <<" Day "<<day<<" would be " <<"May"<< day-120 << endl;


}


if(day >= 152 && day <= 181)

{
cout <<" Day "<<day<<" would be " <<"June" << day -151<< endl;


}


if(day >= 182 && day <= 212)

{
cout <<" Day "<<day<<" would be " <<"July" << day-181 << endl;


}


if(day >= 213 && day <= 243)

{
cout <<" Day "<<day<<" would be " <<"August" << day-212 << endl;


}


if(day >= 244 && day <= 273)

{
cout <<" Day "<<day<<" would be " <<"September" << day -243<< endl;


}


if(day >= 274 && day <= 304)

{
cout <<" Day "<<day<<" would be " <<"October"<< day-273 << endl;



}


if(day >= 305 && day <= 334)

{
cout <<" Day "<<day<<" would be " <<"November"<< day-304 << endl;


}


if(day >= 335 && day <= 365)

{
cout <<" Day "<<day<<" would be " << "December" << day -334<< endl;


}


} // end print function


}; // end Class DayOfYear




int main()

{

// How do I get the main function to display the appropriately formatted month day configuration

// for the day that the user chooses?

int dayEntered;

cout << "Please enter a number from 1 to 365" << endl;

cin >> dayEntered;

DayOfYear instance = DayOfYear (dayEntered);

instance.print();


cin.ignore();

cin.get();


return 0;


}
