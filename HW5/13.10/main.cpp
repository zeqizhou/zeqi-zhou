#include <iostream>

using namespace std;

class NumberArray

{

private:

float* arr;

int numElements;

public:

NumberArray();

NumberArray(int);

void storeNum(int) const;

float getNumber(int) const;

int Size() const;

void highestElement() const;

void lowestElement() const;

void average() const;

~NumberArray();

};

NumberArray::NumberArray(int n)

{

arr = NULL;

numElements = n;

arr = new float[n];

}

int NumberArray::Size() const

{

return numElements;

}

void NumberArray::storeNum(int _size) const

{

float num;



for (int count = 0; count < _size; count++)

{

cout << "Enter the value: ";

cin >> num;

arr[count] = num;

}




}

float NumberArray::getNumber(int num) const

{

return arr[num];

}

void NumberArray::highestElement() const

{

float highest;

highest = arr[0];

for (int count = 0; count < numElements; count++)

{

if (arr[count] > highest)

highest = arr[count];

}

cout << "The highest number is: " << highest << endl;

}

void NumberArray::lowestElement() const

{

float lowest;

lowest = arr[0];

for (int count = 0; count < numElements; count++)

{

if (arr[count] < lowest)

lowest = arr[count];

}

cout << "The lowest number is: " << lowest << endl;

}

void NumberArray::average() const

{

float average = 0.0;

for (int count = 0; count < numElements; count++)

{

average += arr[count];

}

average = average / numElements;

cout << "The average is: " << average << endl;

}

NumberArray::~NumberArray()

{

delete arr;

}

int main()

{

int n;

cout << "Enter the size of array: ";

cin >> n;

NumberArray arr(n);

arr.storeNum(n);

arr.average();

arr.lowestElement();

arr.highestElement();

return 0;

}