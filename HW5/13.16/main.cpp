#include <iostream>

using namespace std;

//global variables
double ethylFreeze = -173;
double ethylBoil = 172;
double waterFreeze = 32;
double waterBoil = 212;
double oxygenFreeze = -362;
double oxygenBoil = -306;

// Class declaration
class FreezeBoil

{

private:

double temperature; // To hold a temperature

public:

// Default constructor

FreezeBoil()

{

temperature = 0;

}

  

// Constructor

FreezeBoil(double t)

{

temperature = t;

}

// Mutator

void setTemperature(double t)

{

temperature = t;

}

// Accessor

double getTemperature() const

{

return temperature;

}

// Other member functions

bool isEthylFreezing()

{
if(temperature<=ethylFreeze)
{
return true;
}

return false;
}

bool isEthylBoiling()

{

if(temperature>=ethylBoil)
{
return true;
}

return false;
}

bool isOxygenFreezing()
{

if(temperature<=oxygenFreeze)
{
return true;
}
  
return false;
}

bool isOxygenBoiling()
{

if(temperature>=oxygenBoil)
{
return true;
}
  
return false;
}

bool isWaterFreezing()

{

if(temperature<=waterFreeze)
{
return true;
}
  
return false;
}

bool isWaterBoiling()
{
if(temperature>=waterBoil)
{
return true;
}
  
return false;
  
}

};

// Main function

int main()

{

double temp;  

// Create a FreezeBoil object.

FreezeBoil substance;

cout << "Enter a temperature: ";

cin >> temp;

// Store the temperature in the object.

substance.setTemperature(temp);

cout << "The following substances freeze at "

<< "that temperature:\n";

if (substance.isEthylFreezing())

cout << "\tEthyl Alcohol\n";

if (substance.isOxygenFreezing())

cout << "\tOxygen\n";

if (substance.isWaterFreezing())

cout << "\tWater\n";

cout << "The following substances boil at "

<< "that temperature:\n";

if (substance.isEthylBoiling())

cout << "\tEthyl Alcohol\n";

if (substance.isOxygenBoiling())

cout << "\tOxygen\n";

if (substance.isWaterBoiling())

cout << "\tWater\n";

  

return 0;

}