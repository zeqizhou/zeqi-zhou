#include<iostream>
#include<stdlib.h>
#include<string>
using namespace std;

class Coin{
   private:
       string sideUp;
   public:
       Coin()
       {
           int x=rand()%2;
           if(x==0)
           sideUp="heads";
           else
           sideUp="tails";
       }
       void toss()
       {
           int x=rand()%2;
           if(x==0)
           sideUp="heads";
           else
           sideUp="tails";
       }
       string getsideUp()
       {
           return sideUp;
       }
};
int main()
{
   Coin C=Coin();
   int head=0;
   int tail=0;
   cout<<"Initial face : "<<C.getsideUp()<<endl;
   string s;
   for(int i=0; i<20; i++)
   {
       C.toss();
       cout<<"Facing : "<<C.getsideUp()<<endl;
       if(C.getsideUp()=="heads")
       head++;
       else
       tail++;
   }
   cout<<"Total heads : "<<head<<" and tails : "<<tail<<endl;
  
  
   return 0;
}