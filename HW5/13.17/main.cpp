#ifndef TAX_H
#define TAX_H
//Tax class declaration
class Tax
{
private: double itemCost;
//cost of the item
double taxRate;
//sales tax rate
public: //pull user input to get itemCost with tax rate 6%
Tax(double subTotal, double rate = 0.06)
{
itemCost = subTotal;
taxRate = rate;
}
//get Item cost return
double getItemCost() const
{
return itemCost;
}
//get tax rate
double getTaxRate() const
{
return taxRate;
}
//get tax times itemCost
double getTax() const
{ return (itemCost * taxRate); }
//get subtotal
double getTotal() const
{
return (itemCost + getTax());
}
};
#endif

#ifndef INVENTORYITEM_H
#define INVENTORYITEM_H
#include <cstring>
//needed for strlen and strcpy
//constant for description's default sizeconst
int DEFAULT_SIZE = 51;
//InventoryItem class declaration
class InventoryItem
{private: char *description;
//item description
double cost; //item cost
double units;
//Number of units on hand
// Private member function.void
InventoryItem::createDescription(int size, char *value)
{
// Allocate the default amount of memory for description.
description = new char [size+1];
// Store a value in the memory.
strcpy(description, value);
}
public:
//constructor #1
InventoryItem()
{
//store an empty string in the description attribute.
createDescription(DEFAULT_SIZE, "");
//initialize cost and units.
cost = 0.0;
units = 0;
}
//constrcutor #2
InventoryItem(char *desc)
{
//allocate memory and store the description.
createDescription(strlen(desc), desc);
//initialize cost and units.
cost = 0.0; units = 0; }
//constructor #3
InventoryItem(char *desc, double c, double u)
{//allocate memory and store the description.
createDescription(strlen(desc), desc);
//assign values to cost and units.
cost = c; units = u;
}
//Destructor
~InventoryItem()
{ delete [] description; }
//Mutator functions
void setDescription(char *d)
{ strcpy(description, d); }
void setCost(double c)
{ cost = c; }
void setUnits(double u)
{ units = u; }
//Accessor functions
const char *getDescription() const
{ return description; }
double getCost() const
{ return cost; }
double getUnits(double qtySelected)
{
units -= qtySelected;
return units;
}
};
#endif
//This program demonstrates a class with a destructor
#include <iostream>
#include <iomanip>
#include "Profit.h"
#include "Tax.h"
#include "InventoryItem.h"
using namespace std;
int main()
{
int numSelected;
//to hold number selected
double qtySelected;
//to hold number of items ordered char again;
//to hold yes or no answer
double profitPrice;
//to hold unit price amount for profit class
double subTotal;
//to hold sub total price for tax class
const int NUM_ITEMS = 5;
//array of inventory items
InventoryItem inventory[] = { InventoryItem("Hammer", 6.95, 12),
InventoryItem("Wrench", 8.75, 20),
InventoryItem("Pliers", 3.75, 10),
InventoryItem("Ratchet", 7.95, 14),
InventoryItem("Screwdriver", 2.50, 22) };
//do loop for program to function
do{
//set numeric output formatting
cout << fixed << showpoint << setprecision(2);
//header to display title of list of inventory
cout << "Here is a list of our current inventory" << endl;
cout << " " << endl; //table format
cout << setw(7) << "Item Number" <<
cout << setw(14) << "Description" << setw(8) << "Cost" << setw(8) <<setw(17) << "Units on Hand\n";
cout << "------------------------------------------------------------\n";
//for loop display conents of array into table format
for (int i = 0; i < NUM_ITEMS; i++)
{ cout << setw(7) << i;
cout << setw(21) << inventory[i].getDescription();
cout << setw(10) << "$" << inventory[i].getCost();
cout << setw(8) << inventory[i].getUnits() << endl;
}
cout << "" << endl;
//spacer cout << "" << endl;
//prompt user to input inventory number purchase
cout << "Please enter the item number you wish to purchase: ";
cin >> numSelected;
//validation loop to ensure number is valid
while((numSelected < 0) || (numSelected > 4))
{
cout << "Please enter an inventory item number 0-4: ";
cin >> numSelected;
}
//prompt user for quantity of selection
cout << "Please enter the quantity you wish to purchase: ";
cin >> qtySelected;
//validation loop to ensure number is valid
while(qtySelected > inventory[numSelected].getUnits())
{ cout << "Please enter quantity less than or equal to " << inventory[numSelected].getUnits() << " units: ";
cin >> qtySelected;
} //ensure a reference of InventoryItem object is selected to profit class
profitPrice = inventory[numSelected].getCost();
cout << " " << endl;
//spacer
//display subtotal, tax, and total
cout << "Here is the bill: " << endl; cout << " " << endl;
//spacer
//create a sale object for this transaction
//specify the item cost, but use the default
//tax rate of 6 percent
Profit itemSale2(profitPrice);
subTotal = itemSale2.getTotalUnitPrice() * qtySelected;
cout << "Subtotal:\t $" << subTotal << endl;
//create a price object for this transaction for the tax class
//specify the subtotal
//subTotalCost = subTotal;
Tax itemSale(subTotal);
//display results
cout << "Sales tax:\t $" << itemSale.getTax() << endl;
cout << "Total:\t\t $" << itemSale.getTotal() << endl;
//prompt user if another purchase is to be made
cout << "Do you want to make another purchase? "; cin >> again;
cout << " " << endl;}
//while loop to control if another purchase is going to be made
}while (again == 'Y' || again == 'y');
return 0;
}