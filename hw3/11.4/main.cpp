#include <iostream>
#include <string>
using namespace std;


struct Moviedata
{
string movieTitle;
string movieDirector;
int yearReleased;
int runningtime;
double movieProductionCosts;
double FirstYearRevenues;
};

void display(Moviedata movie)
{
cout<<"\n\nMovie Details:\n";
cout<<"-----------------\n";
cout<<"Title: "<<movie.movieTitle<<endl;
cout<<"Director: "<<movie.movieDirector<<endl;
cout<<"Year Released: "<<movie.yearReleased<<endl;
cout<<"Running Time "<<movie.runningtime<<endl;
cout<<"First Year's Profit: "<<movie.movieProductionCosts - movie.FirstYearRevenues<<endl;
}

int main()
{
Moviedata movie;
cout <<"Enter the following data about a movie:\n";

cout << "Title: ";
getline(cin,movie.movieTitle);

cout << "Director: ";
getline(cin,movie.movieDirector);

cout << "Year Released: ";
cin>>movie.yearReleased;

cout << "Running time: ";
cin>>movie.runningtime;

cout << "Production Costs: ";
cin>>movie.movieProductionCosts;

cout << "First-Year Revenues: ";
cin>>movie.FirstYearRevenues;

display(movie);
}