#include<iostream>

#include<string>

using namespace std;

struct weather

{

double totalRainfall;

double highTemp;

double lowTemp;

double avgTemp;

};

int main()

{

weather months[12];

double total =0, highest, lowest, avgsum;

int highmonth, lowmonth;

string month[12]= {"January", "February", "March", "April", "May", "June",

"July", "August", "September", "October", "November",

"December"};

for(int i=0; i<12; i++)

{

cout<<"Enter total rainfall for month "<<month[i]<<": ";

cin>>months[i].totalRainfall;

cout<<"Enter high temperature: ";

cin>>months[i].highTemp;

while (months[i].highTemp < -100 || months[i].highTemp > 140)

{

cout<<"ERROR: Temperature must be in the range of "

<<"-100 through 140.\n";

cout<<"\tHigh Temperature: ";

cin>>months[i].highTemp;

}

cout<<"Enter low temperature: ";

cin>>months[i].lowTemp;

while (months[i].lowTemp < -100 || months[i].lowTemp > 140)

{

cout<<"ERROR: Temperature must be in the range of "

<<"-100 through 40.\n";

cout<<"\tLow Temperature: ";

cin>>months[i].lowTemp;

}

}

//Calculate the monthly average temperature and total rainfall.

for (int i=0; i<12; i++)

{

total=total + months[i].totalRainfall;

months[i].avgTemp= (months[i].highTemp + months[i].lowTemp)/2;

}

highest=months[0].highTemp;

lowest=months[0].lowTemp;

for(int i=1; i<12; i++)

{

if(months[i].highTemp>highest)

{

highest=months[i].highTemp;

highmonth=i;
cout<<highmonth;

}

if(months[i].lowTemp<lowest)

{

lowest=months[i].lowTemp;

lowmonth=i;
cout<<highmonth;

}

}

avgsum = 0;

//Calculate the average.

for(int i=0; i<12; i++)

{

avgsum=avgsum+months[i].avgTemp;

}

//Display the calculated data.

cout<<"Average monthly rainfall: "<<total/12<<endl;

cout<<"Total monthly rainfall: "<<total<<endl;

cout<<"Highest rainfall in "<<month[highmonth]<<"is: "<<highest<<endl;

cout<<"Lowest rainfall in "<<month[lowmonth]<<"is: "<<lowest<<endl;

cout<<"Average of average temperatures is: "<<avgsum/12<<endl;

return 0;

}