#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int *create_array(int size) {
   return new int[size];
}

void populate_array(int *arr, int size) {
   for(int i = 0; i < size; ++i) {
      arr[i] = rand() % 100;
   }
}

void print_array(int *arr, int size) {
   cout << "Array is: ";
   for(int i = 0; i < size; ++i) {
      cout << arr[i] << " ";
   }
   cout << endl;
}

int main() {
   srand(time(NULL));
   int size = 10;
   int *arr = create_array(size);
   populate_array(arr, size);
   print_array(arr, size);
   delete[] arr;
   return 0;
}