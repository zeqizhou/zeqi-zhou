#include <iostream>
#include <string>

using namespace std;

string* deleteEntry(string *array, int& size, int loc) {
   if(loc >= 0 && loc < size) {
      string *newArray = new string[size];
      int index = 0;
      for(int i = 0; i < size; ++i) {
         if(i != loc) {
            newArray[index++] = array[i];
         }
      }
      size--;
      return newArray;
   } else {
      return array;
   }
}

void print_array(string *arr, int size) {
   for(int i = 0; i < size; ++i) {
      cout << arr[i] << " ";
   }
   cout << endl;
}

int main() {
   int size = 4;
   string *arr = new string[size];
   arr[0] = "ronaldo";
   arr[1] = "messi";
   arr[2] = "bale";
   arr[3] = "isco";
   print_array(arr, size);
   arr = deleteEntry(arr, size, 2);   // delete bale
   print_array(arr, size);
   arr = deleteEntry(arr, size, 1);   // delete messi
   print_array(arr, size);
   arr = deleteEntry(arr, size, 10);  // invalid loc
   print_array(arr, size);
   return 0;
}