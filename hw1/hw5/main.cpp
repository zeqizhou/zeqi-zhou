#include<iostream>
 
#include <cstdlib> 


using namespace std;

//function declarations

double Average(double *scores, int number);

void sortTestScores(double *scores, int n);

int main()

{

     double *testScores;

     double avgScore = 0;

     int number, i;

     //prompt the user to enter number of test scores

     cout << "Enter number of test Scores: ";

     //read number of test scores

     cin >> number;

     //dynamically allocate memory to store test scores

     testScores = new double[number];

     //read all test scores

     for (i = 0; i<number; i++)

     {

          cout << "Enter Test score " <<i+1<<": " ;

          cin >> *(testScores+i);

          //if negative score is entered, prompt the user

          //to enter positive score

          while (*(testScores + i) < 0)

          {

              cout << "Sorry! Only positive numbers are accepted "<<endl;

              cout << "Enter Test score " << i + 1 << ": ";

              cin >> *(testScores + i);

          }

     }

     //pass test scores to sort

     sortTestScores(testScores, number);

     cout << "Sorted TestScores:"<<endl;

     for (i = 0; i<number; i++)

     {

          cout << *(testScores + i) << endl;

     }

     avgScore = Average(testScores, number);

     cout << "Average of test scores is:" << avgScore << endl;

     system("pause");

     return 0;

}

//sorts the given test socres

void sortTestScores(double *scores, int number)

{

     double temp;

     int i, j;

     //sort the scores

     for (i = 1; i<number; i++)

     {

          for (j = 0; j<number - i; j++)

          {

              if (scores[j]>scores[j + 1])

              {

                   temp = scores[j];

                   scores[j] = scores[j + 1];

                   scores[j + 1] = temp;

              }

          }

     }

}

//finds the average of test scores after dropping lowest score

double Average(double *score, int n)

{

     int i;

     double sum=0.0,avg = 0.0;

     //Since the socres are sorted in ascending order,

     //always first score is the lowest score

     double lowest = score[0];

     //find the sum of all test scores

     for (i = 0; i<n; i++)

     {

          sum += score[i];

     }

     //Drop lowest score

     sum=sum - lowest;

     cout << "Lowest score " << lowest << " is droped from sum" << endl;

     //calculate average of test scores without lowest score

     avg = sum / (n-1);

     return avg;

}