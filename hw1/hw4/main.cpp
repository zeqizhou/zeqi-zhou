//Header file declaration

#include<iostream>

#include<stdlib.h>

//function prototypes

using namespace std;

int sort(int* array, int size);

float average(int * array, int size);

//starting of main function

int main()

{

//variable data type declaration

int size, i;

float avg;

int *p;

//asking for user input

cout<<"Please enter the number of test scores: ";

cin>>size;

//memory allocation for the array

int *array = new int[size];

cout<<"Please enter the "<<size<<" scores: "<<endl;

for(i=0;i<size;i++)

{

cin>>*(array+i);

if(*(array+i)<0)

{
cout<<"Please enter only positive scores: ";

cin>>*(array+i);

}

}

//Display array elements before sorting

cout<<"Scores before sorting are:\n"<<endl;

for(i=0;i<size;i++)

{

cout<<*(array+i)<<" ";

}

cout<<endl;

//call to functios

sort(array, size);

//call to function

avg = average(array, size);

//Display Array

cout<<"Average of all the scores is:\n"<<avg<<endl;

//display sorted array

cout<<"Scores after sorting are:\n"<<endl;

for(i=0;i<size;i++)

{

cout<<*(array+i)<<" ";

}

cout<<endl;

return 0;

}

//function definition

float average(int * array, int size)
{

int i;

float avg;

float sum = 0;

for(i=0;i<size;i++)

{
//calculate sum

sum = sum + *(array+i);

}

//calculate avrerage

avg = sum/size;

return avg;

}

//function to sort the array element

int sort(int* array, int size)

{

int i, j, temp;

for(i = 0; i < size; ++i)
{

for(j = i + 1; j < size; ++j)

{

if(*(array+i) > *(array+j))

{

temp = *(array+i);

*(array+i) = *(array+j);

*(array+j) = temp;

}

}

}

}//end of the program