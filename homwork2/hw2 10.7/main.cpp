#include <iostream>
#include <cstring>
using namespace std;
int main() {
//Declare the character arrays
char firstName[50], middleName[50], lastName[50];
char fullName[150];
//Get the input from the user
cout << "Enter First Name: ";
cin >> firstName;
cout << "Enter Middle Name: ";
cin >> middleName;
cout << "Enter Last Name: ";
cin >> lastName;
//Copy the input to the destination array
strcpy(fullName, lastName);
//Concatenate the rest of the values
strcat(fullName, ", ");
strcat(fullName, firstName);
strcat(fullName, " ");
strcat(fullName, middleName);
//Display the result
cout << "Full Name: " << fullName;
}