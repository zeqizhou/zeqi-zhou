#include <iostream>
 
#include <cstdlib> 


using namespace std;
int *reverse(int[], int);
void print(int[], int);
int main()
{int size=10, array[10]={1,2,3,4,5,6,7,8,9,10};
cout<<"Original array\n";
print(array,size);
int *reversedArray=reverse(array, size);
cout<<"Reversed array\n";
print(reversedArray, size);
system("pause");
cin.get();
return 0;
}
int *reverse(int a[],int n)
{int i,j;
if(n<=0)
    return NULL;
int *copy = new int[n];
for(i=0;i<n;i++)
    copy[i]=a[n-i-1];
return copy;
}
void print(int a[],int n)
{int i;
for(i=0;i<n;i++)
        cout<<a[i] << " ";
cout << endl;
}